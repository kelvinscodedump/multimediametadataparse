﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using static MultimediaMetadataParse.MultimediaParsing;

namespace MultimediaMetadataParseExec
{
    public static class MainClass
    {
        public static void Main(string[] args)
        {
            List<string> mp4s = Directory.EnumerateFiles(args[0], "*.mp4", SearchOption.TopDirectoryOnly).ToList();

            foreach(string mp4Path in mp4s)
            {
                try
                {
                    MP4_Metadata file = new(mp4Path);
                    file.GetAllBoxes();

                    Console.WriteLine($"{mp4Path}\n");

                    Console.WriteLine
                    (
                        "  ╔══════════════╦══════════════╦══════════════╦══════════════╗\n" +
                        "  ║ Type         ║ Box StartPos ║ Box EndPos   ║ Length       ║\n" +
                        "  ╠══════════════╬══════════════╬══════════════╬══════════════╣"
                    );

                    static void PrintSubBoxes(Box box, int layer)
                    {
                        layer++;

                        foreach(Box subBox in box.SubBoxes)
                        {
                            Console.WriteLine($"  ║ {("└" + subBox.Type).PadLeft(4 + layer),-12} ║ {subBox.Index,-12} ║ {subBox.Index + subBox.Length,-12} ║ {subBox.Length,-12} ║");

                            PrintSubBoxes(subBox, layer);

                        }
                    }

                    foreach(Box box in file.TopLevelBoxes)
                    {
                        Console.WriteLine($"  ║ {box.Type,-12} ║ {box.Index,-12} ║ {box.Index + box.Length,-12} ║ {box.Length,-12} ║");

                        PrintSubBoxes(box, 0);

                    }

                    Console.WriteLine
                    (
                        "  ╚══════════════╩══════════════╩══════════════╩══════════════╝\n"
                    );

                    foreach(Track s in file.Tracks)
                    {
                        Console.WriteLine($"  {s.Codec}");
                    }

                    Console.WriteLine();
                }

                catch { }
                finally { }
            }
        }
    }
}

