﻿using System.Collections.Generic;

namespace MultimediaMetadataParse
{
    public partial class MultimediaParsing
    {
        public static readonly Dictionary<string, int> BoxTypes = new()
        {
            {"moov", 0},
            {"trak", 0},
            {"edts", 0},
            {"mdia", 0},
            {"minf", 0},
            {"stbl", 0},
            {"mvex", 0},
            {"moof", 0},
            {"traf", 0},
            {"mfra", 0},
            {"skip", 0},
            {"meta", 0},
            {"stsd", 1 }
        };

        public static readonly Dictionary<string, string> Codecs = new()
        {
            {"avc1", "H.264"},
            {"avcC", "H.264"},
            {"hvc1", "H.265"},
            {"hev1", "H.265"},
            {"mp4a", "MPEG-4A"},
            {"ac-3", "AC-3" },
        };
    }
}
