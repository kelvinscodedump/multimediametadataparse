﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MultimediaMetadataParse
{
    public partial class MultimediaParsing
    {               
        public class Box
        {
            public long Index { get; set; }
            public string Type { get; set; }
            public long Length { get; set; }

            public byte[] Length64
            {
                set
                {
                    if(BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(value);
                    }

                    Length = BitConverter.ToInt64(value);
                }
            }

            public List<Box> SubBoxes { get; set; }

            public Box()
            {
                Index = 0;
                Length = 0;
                Type = string.Empty;

                SubBoxes = new();
            }

            public Box(long index, byte[] len, byte[] type) : this()
            {
                Index = index;
                Type = Encoding.ASCII.GetString(type);

                if(BitConverter.IsLittleEndian)
                {
                    Array.Reverse(len);
                }

                Length = BitConverter.ToInt32(len);
            }
        }

        public class Track
        {
            public Track(int num, string cod)
            {
                Number = num;
                Codec = cod;
            }

            public int Number { get; set; }
            public string Codec { get; private set; } = string.Empty;
        }

        public class MP4_Metadata
        {
            public MP4_Metadata(string fileString)
            {
                TopLevelBoxes = new();
                Path = fileString;
                Tracks = new();                
            }

            public long Length { get; set; }
            public string Path { get; set; }
            public List<Box> TopLevelBoxes { get; set; }
            public List<Track> Tracks { get; set; }

            public void GetTopLevelBoxes()
            {
                using(FileStream mp4Stream = File.OpenRead(Path))
                {
                    TopLevelBoxes = GetBoxes(mp4Stream, 0, mp4Stream.Length);
                }
            }

            public void GetAllBoxes()
            {
                using(FileStream mp4Stream = File.OpenRead(Path))
                {
                    TopLevelBoxes = GetBoxes(mp4Stream, 0, mp4Stream.Length);

                    for(int i = 0; i < TopLevelBoxes.Count; i++)
                    {
                        Box tlBox = TopLevelBoxes[i];
                        GetSubBoxes(mp4Stream, tlBox);
                    }
                }
            }

            public void GetSubBoxes(FileStream mp4Stream, Box inBox)
            {
                if(BoxTypes.TryGetValue(inBox.Type, out int value))
                {
                    if(value == 0)
                    {
                        inBox.SubBoxes = GetBoxes(mp4Stream, inBox.Index + 8, inBox.Index + inBox.Length);

                        for(int i = 0; i < inBox.SubBoxes.Count; i++)
                        {
                            Box subBox = inBox.SubBoxes[i];
                            GetSubBoxes(mp4Stream, subBox);
                        }
                    }

                    if(value == 1)
                    {
                        byte[] readBytes = new byte[4];

                        mp4Stream.Seek(inBox.Index + 20, SeekOrigin.Begin);
                        mp4Stream.Read(readBytes, 0, 4);

                        if(Codecs.TryGetValue(Encoding.ASCII.GetString(readBytes), out string codec))
                        {
                            Track track = new(1, codec);
                            Tracks.Add(track);
                        }
                    }
                }
            }

            public List<Box> GetBoxes(FileStream mp4Stream, long startIndex, long endIndex)
            {
                long readByteUpto = startIndex;
                byte[] readBytes = new byte[8];
                List<Box> boxList = new();

                mp4Stream.Seek(startIndex, SeekOrigin.Begin);

                while(endIndex - readByteUpto > 0)
                {
                    mp4Stream.Read(readBytes, 0, 8);

                    (byte[] sizeBytes, byte[] typeBytes) = SplitArray(readBytes);

                    Box box = new(readByteUpto, sizeBytes, typeBytes);

                    boxList.Add(box);

                    long headerOffset = box.Length - 8;

                    switch(box.Length)
                    {
                        case 0:

                            headerOffset = mp4Stream.Length;

                            break;

                        case 1:

                            mp4Stream.Read(readBytes, 0, 8);
                            box.Length64 = readBytes;
                            headerOffset = box.Length - 16;

                            break;
                            
                        default:

                            break;
                    }

                    readByteUpto = mp4Stream.Seek(headerOffset, SeekOrigin.Current);
                    readBytes = new byte[8];
                }

                return boxList;
            }
        }

        public static (T[], T[]) SplitArray<T>(T[] inArray, int splitIndex)
        {
            T[] ar0 = new T[splitIndex];
            T[] ar1 = new T[inArray.Length - splitIndex];

            Array.Copy(inArray, ar0, splitIndex);
            Array.Copy(inArray, splitIndex, ar1, 0, inArray.Length - splitIndex);

            return (ar0, ar1);
        }

        public static (T[], T[]) SplitArray<T>(T[] inArray)
        {
            return SplitArray(inArray, inArray.Length / 2);
        }
    }
}